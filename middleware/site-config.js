export default function ({store, route}) {
    store.dispatch('podcasts/getPodcastsBySite', route.params.site);    
}