export const state = () => ({
    podcasts: null
})

export const mutations = {

}

export const actions = {
    getPodcastsBySite({state, commit}, site) {
        commit(`setSiteName`, site)
        this.$axios.$get(`/api/v1/podcast/podcasts?site=${state.actualSite.domain}&publicado=1&ativo=1`).then((response) => {
            commit(`importData`, response)
        })
    }
}

export const getters = {

}