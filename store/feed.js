export const state = () => ({
    feed: null,
    podcastId : ""
})

export const mutations = {
    importData(state, data) {
        state.feed = data
        state.podcastId = data.id
    },
    resetData(state) {
        state.feed  = null
        state.podcastId = ""
    }
}

export const actions = {
    getPodcastById({commit}, id) {
        commit(`resetData`)
        this.$axios.$get(`/podcast/feed/${id}.json`).then((response) => {
            response.id = id
            commit(`importData`, response)
        })
    }
}

export const getters = {
    get(state) {
        return state.feed
    },
    getImageAudio(state) {
        return (audio) => {
            if (typeof audio['itunes:image'] !== "undefined") {
                return audio['itunes:image']['@href']
            }
            return state.feed.rss.channel.image.url
        }
    }
}