export const state = () => ({
    boxes: [
        {
            layout: {
                slug: "",
            }
        }
    ]
})

export const mutations = {
    importBoxes(state, data) {
        state.boxes = data.widgets
    }
}

export const actions = {
    getHomeBySite(context, api) {
        this.$axios.$get(`${api}`).then((response) => {
            context.commit(`importBoxes`, response)
        })
    }
}

export const getters = {
    get(state) {
        return state.boxes
    }
}