export const state = () => ({
    podcasts: null,
    actualSite: null,
    sites: [
        {
            domain: 'cbn.globoradio.globo.com',
            slug: 'cbn',
            color: '#e11b22',
            logo: '/logos/cbn.png',
            apiHome: 'http://publicacao.globoradio.globo.com/api/v1/aplicativos/1/paginas/5'
        },
        {
            domain: 'radioglobo.globo.com',
            slug: 'radioglobo',
            color: '#181818',
            logo: '/logos/radioglobo.png',
            apiHome: 'http://publicacao.globoradio.globo.com/api/v1/aplicativos/3/paginas/7'
        },
        {
            domain: 'bhfm.globo.com',
            slug: 'bhfm',
            color: '#000000',
            logo: '/logos/bhfm.png'
        },
        {
            domain: 'globoesporte.com',
            slug: 'globoesporte',
            color: '#3b9c00',
            logo: '/logos/globoesporte.png'
        },
        {
            domain: 'g1.globo.com',
            slug: 'g1',
            color: '#c4170c',
            logo: '/logos/g1.png'
        },
        {
            domain: 'gshow.globo.com',
            slug: 'gshow',
            color: '#ec7d00',
            logo: '/logos/gshow.png'
        },
        {
            domain: 'gnt.globo.com',
            slug: 'gnt',
            color: '#37604e',
            logo: '/logos/gnt.png'
        },
    ]
})

export const mutations = {
    importData(state, data) {
        state.podcasts = data
    },
    setSiteName(state, site) {
        state.actualSite =  (state.sites.filter(function(obj){
            if(obj.slug==site) return obj;
        }))[0]
    }
}

export const actions = {
    getPodcastsBySite({state, commit}, site) {
        commit(`setSiteName`, site)
        this.$axios.$get(`/api/v1/podcast/podcasts?site=${state.actualSite.domain}&publicado=1&ativo=1`).then((response) => {
            commit(`importData`, response)
        })
    }
}

export const getters = {
    get(state) {
        return state.podcasts
    },
    getActualSite(state) {
        return state.actualSite
    },
    getSiteByDomain(state){
        return (domain) => {
            return state.sites.filter(function(obj){
                if(obj.domain==domain) return obj;
            })[0]
        }
    },
}