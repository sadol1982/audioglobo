export const state = () => ({
    podcastsRelacionados: {
    }
})

export const mutations = {
    importData(state, data) {
        state.podcastsRelacionados = data
    }
}

export const actions = {
    getRelacionadosById({commit}, id) {
        this.$axios.$get(`/api/v1/podcast/${id}/relacionados?publicado=True&ativo=True&ordem=-modificado_data&qtd=5`).then((response) => {
            commit(`importData`, response)
        })
    }
}

export const getters = {
    get(state) {
        return state.podcastsRelacionados
    }
}